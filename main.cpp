#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <string>
using namespace std;

int main(int argc,char* argv[]) {
    long long x;
    long long y;
    int z = 0;
    if(argc == 1){
        srand (time(NULL));
        x = rand() % 9223372036854775807;
    } else {
        x = stoll(argv[1]);
    }

    cout << "Num: " << x << endl;
    y = x;

    while(y != 1){
        if(y % 2 == 0){
            y /= 2;
        } else {
            y *= 3;
            y++;
        }
        cout << y << endl;
        z ++;
    }

    cout << "Steps: " << z << endl;
    return 0;
} 
